#!/bin/bash

# Start Redis instances
redis-server --port 6480 --daemonize yes --requirepass password123 --masterauth password123
redis-server --port 6481 --daemonize yes --replicaof localhost 6480 --requirepass password123 --masterauth password123
redis-server --port 6482 --daemonize yes --replicaof localhost 6480 --requirepass password123 --masterauth password123

# Wait for Redis instances to start
sleep 5

echo "Redis setup completed!"

# Start Redis Sentinels
redis-sentinel redis-sentinel-26480.conf --sentinel --daemonize yes
redis-sentinel redis-sentinel-26481.conf --sentinel --daemonize yes
redis-sentinel redis-sentinel-26482.conf --sentinel --daemonize yes

echo "Redis Sentinel setup completed!"
